package org.bitbucket.es4gwt.shared.elastic;

/**
 * @author Mikael Couzic
 */
public interface ElasticRequestElement {

	String toRequestString();

}
