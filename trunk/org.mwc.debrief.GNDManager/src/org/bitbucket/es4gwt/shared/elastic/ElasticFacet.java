package org.bitbucket.es4gwt.shared.elastic;

/**
 * !!! IMPORTANT !!! For the moment, ElasticFacets must be defined as Enums
 * 
 * @author Mikael Couzic
 */
public interface ElasticFacet extends ElasticRequestElement {

	String name();

}
