package org.bitbucket.es4gwt.shared.elastic.filter;

import org.bitbucket.es4gwt.shared.elastic.ElasticRequestElement;

/**
 * @author Mikael Couzic
 */
public interface ElasticFilter extends ElasticRequestElement {

}
