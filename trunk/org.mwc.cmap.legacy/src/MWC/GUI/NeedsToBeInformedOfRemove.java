package MWC.GUI;


/** interface for objects that need to be informed when they are being removed from the Layers object
 *
 */

public interface NeedsToBeInformedOfRemove
{

	/** this object is being removed from the Layers
	 * 
	 */
	void beingRemoved();

}
