package MWC.GenericData;

public interface ColoredWatchable
{
	/**
	 * find out the default colour for this object
	 */

	java.awt.Color getColor();
}
